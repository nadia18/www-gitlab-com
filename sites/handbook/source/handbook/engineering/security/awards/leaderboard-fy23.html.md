---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vitallium](https://gitlab.com/vitallium) | 1 | 900 |
| [@kassio](https://gitlab.com/kassio) | 2 | 600 |
| [@brodock](https://gitlab.com/brodock) | 3 | 600 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 4 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 5 | 600 |
| [@leipert](https://gitlab.com/leipert) | 6 | 500 |
| [@.luke](https://gitlab.com/.luke) | 7 | 500 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 8 | 500 |
| [@ratchade](https://gitlab.com/ratchade) | 9 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 10 | 500 |
| [@jlear](https://gitlab.com/jlear) | 11 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 12 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 13 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 14 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 15 | 460 |
| [@garyh](https://gitlab.com/garyh) | 16 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 17 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 18 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 19 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 20 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 21 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 22 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 23 | 150 |
| [@10io](https://gitlab.com/10io) | 24 | 100 |
| [@pshutsin](https://gitlab.com/pshutsin) | 25 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 26 | 100 |
| [@jerasmus](https://gitlab.com/jerasmus) | 27 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 28 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 29 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 30 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 31 | 60 |
| [@minac](https://gitlab.com/minac) | 32 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 33 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 34 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 35 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 36 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 37 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 38 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 39 | 30 |
| [@subashis](https://gitlab.com/subashis) | 40 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 41 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 42 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 43 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 44 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 45 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 46 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@f_santos](https://gitlab.com/f_santos) | 1 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 2 | 300 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 3 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@spirosoik](https://gitlab.com/spirosoik) | 1 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 2 | 200 |
| [@zined](https://gitlab.com/zined) | 3 | 200 |

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vitallium](https://gitlab.com/vitallium) | 1 | 900 |
| [@kassio](https://gitlab.com/kassio) | 2 | 600 |
| [@brodock](https://gitlab.com/brodock) | 3 | 600 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 4 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 5 | 600 |
| [@leipert](https://gitlab.com/leipert) | 6 | 500 |
| [@.luke](https://gitlab.com/.luke) | 7 | 500 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 8 | 500 |
| [@ratchade](https://gitlab.com/ratchade) | 9 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 10 | 500 |
| [@jlear](https://gitlab.com/jlear) | 11 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 12 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 13 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 14 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 15 | 460 |
| [@garyh](https://gitlab.com/garyh) | 16 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 17 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 18 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 19 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 20 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 21 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 22 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 23 | 150 |
| [@10io](https://gitlab.com/10io) | 24 | 100 |
| [@pshutsin](https://gitlab.com/pshutsin) | 25 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 26 | 100 |
| [@jerasmus](https://gitlab.com/jerasmus) | 27 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 28 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 29 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 30 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 31 | 60 |
| [@minac](https://gitlab.com/minac) | 32 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 33 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 34 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 35 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 36 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 37 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 38 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 39 | 30 |
| [@subashis](https://gitlab.com/subashis) | 40 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 41 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 42 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 43 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 44 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 45 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 46 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@f_santos](https://gitlab.com/f_santos) | 1 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 2 | 300 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 3 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@spirosoik](https://gitlab.com/spirosoik) | 1 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 2 | 200 |
| [@zined](https://gitlab.com/zined) | 3 | 200 |


