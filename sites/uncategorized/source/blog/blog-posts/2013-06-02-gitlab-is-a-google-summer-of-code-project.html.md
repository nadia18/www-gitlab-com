---
title: GitLab is a Google Summer of Code project
description: "This summer Achilleas Pipinellis will work on packaging GitLab for Fedora and RedHat in a Google Summer of Code project."
canonical_path: "/blog/2013/06/02/gitlab-is-a-google-summer-of-code-project/"
tags: untagged
categories: company
status: publish
type: post
published: true
meta:
  publicize_twitter_user: gitlabhq
  _wpas_done_1532919: '1'
  _publicize_done_external: a:1:{s:7:"twitter";a:1:{i:776273100;b:1;}}
  _elasticsearch_indexed_on: '2013-06-02 14:08:29'
---
This summer Achilleas Pipinellis will work on packaging GitLab for Fedora and RedHat in a Google Summer of Code project. We are very excited and thankful for the support of Fedora and Google. Please see  [Axilleas his blog post on GitLab.org](/blog/2013/05/28/packaging-gitlab-for-fedora-a-gsoc-2013-project/) for more information.
